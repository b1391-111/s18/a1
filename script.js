function PokemonTrainer(name, age, pokemon, friends){
    this.name = name;
    this.age = age;
    this.pokemon = pokemon;
    this.friends = friends;

    this.talk = function(){
        console.log(`Pikachu! I Choose you!`)
    }
}

let pokemons = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
let ashFriends = {
    heonn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
}

let ash = new PokemonTrainer("Ash Ketchum", 19, pokemons, ashFriends);

// New trainer object
console.log(ash);

// Name
console.log(`Result of dot notation:`);
console.log(ash.name);

// Pokemons
console.log(`Result of sqaure bracket notation:`);
console.log(ash["pokemon"]);

// Talk Function
console.log(`Result of talk function:`);
console.log(ash.talk());


// #8
function Pokemon(name, lvl, hp, atk){
    this.name = name;
    this.level = lvl;
    this.health = hp;
    this.attack = atk;

    this.tackle = function(opponent){
        opponent.health -= this.attack;
        console.log(`${name} hero tackled ${opponent.name}`);

        console.log(`${opponent.name}'s health is now reduced to ${opponent.health}`);

        if(opponent.health <= 0){
            opponent.faint();
        }
    }

    this.faint = function() {
        console.log(`${name} Fainted`);
    }
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon("Geodude", 8, 16, 8);
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);

console.log(geodude)
console.log(mewtwo.tackle(geodude));
console.log(geodude)